from datetime import datetime
from os import path

import numpy as np
from PIL import Image

import torch
from torchvision import transforms
from torchvision.datasets import ImageFolder
import torch.optim as optim

from efficientnet_pytorch import EfficientNet

from poutyne.framework import Experiment
from poutyne.framework.callbacks import lr_scheduler as sched

class Classifier(object):

    def __init__(self, scale=0, channels=1, img_size=(32,32)):
        """
        scale: the size of the EfficientNet model (int)
        channels: the numbers of image channels to train on
        img_size: what size to transform them image to for training
        """
        self.scale = scale
        self.device = torch.device('cuda' if torch.cuda.is_available() \
                                          else 'cpu')
        self.no_channels = channels
        self.img_size = img_size

    def infer(self, img):
        """
        img: either a path to an image or a PIL.Image object
        """
        img = Image.open(img) if type(img) is str else img
        tns = self.transforms(img)
        tns = tns.unsqueeze(0)
        preds = self.exp.model.predict(tns)
        return self.classes[np.argmax(preds)]

    def train(self, dataset, run_name, epochs=15, lr=0.01, momentum=0.9,
              batch_size=1024, trans=None):
        """
        Train the EfficientNet on a torchvision dataset.
        The training loop is powered by poutyne for simplicity.
        Logs, checkpoints etc are stored in `./runs/name_of_dataset_class`

        dataset: any class from `torchvision.datasets`
                 (e.g. torchvision.datasets.FashionMNIST)
                 OR a path to a custom dataset
                 This should be a path to a folder containing a 'train' and
                 'test' folder, where each is  configured in line with
                 `torchvision.datasets.ImageFolder`
        run_name: the name to store the training session as
        epochs: number of epochs to run the training for
        lr: learning rate
        momentum: momentum for SGD
        batch_size: number of images per mini-batch
        trans: list of `torchvison.transforms`, if empty, a default set of
               transforms are used
        """
        self._set_transforms(trans)

        # Get the datasets
        if type(dataset) == str:
            trainset, testset = self._load_custom_dataset(dataset)
        else:
            trainset, testset = self._load_tv_dataset(dataset)

        # Convert to DataLoader type
        trainloader, testloader = self._load_data(trainset, testset, batch_size)

        # Run the train cycle
        self.model = EfficientNet.from_pretrained(
                        f"efficientnet-b{self.scale}",
                        advprop=True,
                        in_channels=self.no_channels,
                        num_classes=len(self.classes),
                    )

        self.exp = Experiment(f"./runs/{run_name}",
                        self.model,
                        optimizer=optim.SGD(self.model.parameters(),
                                            lr=lr, momentum=momentum),
                        task='classif',
                        device=self.device)
        lr_scheduler = sched.CosineAnnealingLR(epochs)
        self.exp.train(trainloader, testloader, epochs=epochs,
                       lr_schedulers=[lr_scheduler])

    def _load_custom_dataset(self, root_dir):
        trainset = self._build_custom_dataset(path.join(root_dir, "train"))
        testset = self._build_custom_dataset(path.join(root_dir, "test"))
        return trainset, testset

    def _load_tv_dataset(self, dataset):
        trainset = dataset(root='./data', train=True,
                           download=True, transform=self.transforms)
        testset = dataset(root='./data', train=False,
                          download=True, transform=self.transforms)
        return trainset, testset


    def _load_data(self, trainset, testset, batch_size, root="./data"):
        """
        trainset: an instance of torchvision.
        batch_size: the size of a mini-batch
        root: path to where the dataset is stored/to be downloaded
        """

        self.classes = trainset.classes

        trainloader = torch.utils.data.DataLoader(trainset,
                                                  batch_size=batch_size,
                                                  shuffle=True, num_workers=2)
        testloader = torch.utils.data.DataLoader(testset,
                                                 batch_size=batch_size,
                                                 shuffle=False, num_workers=2)

        return trainloader, testloader

    def _set_transforms(self, trans):
        if trans is None:
            # Default set of transforms for FashionMNIST
            self.transforms = transforms.Compose([
                    transforms.Grayscale(self.no_channels),
                    transforms.Resize(self.img_size),
                    transforms.ToTensor(),
                ])
        else:
            self.transforms = transforms.Compose(transforms)

    def _build_custom_dataset(self, root_dir):
        return ImageFolder(root_dir, transform=self.transforms,
                           loader=Image.open)
