import os
import json
from warnings import warn

from utils import threaded, gcp_auth

from kafka import KafkaConsumer, KafkaProducer
from google.cloud import pubsub_v1


class UnifiedPubSubAPI(object):

    def __init__(self, fn,
                 kaf_sub_host="localhost", kaf_sub_port=9092,
                 kaf_pub_host="localhost", kaf_pub_port=9092,
                 kaf_sub_topic=None, kaf_pub_topic=None,
                 gcp_project=None,
                 gcp_sub_topic=None, gcp_pub_topic=None):
        """
        A unified api for handling pub/sub data from GCP PubSub and Kafka

        N.B. This class does not create the necessary topics/subscriptions on
             the platforms. You will need to configure those manually.

        fn: the function to call on the incoming datasets
        kaf_sub_host: the address to the kafka host to subscribe to
        kaf_sub_port: the port of the `kaf_sub_host` to connect to
        kaf_pub_host: the address of the Kafka host to publish to
        kaf_pub_port: the port of the `kaf_pub_host` to connect to
        gcp_project: the name of the project the GCP topics/subscribers are located
        gcp_sub_topic: the GCP subcsription to subscribe to
        gcp_pub_topic: the GCP topic to publish to
        """

        self.fn = fn

        if kaf_sub_topic and kaf_pub_topic:
            self.kafka = KafkaPubSub(sub_topic=kaf_sub_topic,
                                     pub_topic=kaf_pub_topic,
                                     fn=self._kafka_message_in,
                                     sub_host=kaf_sub_host,
                                     sub_port=kaf_sub_port,
                                     pub_host=kaf_pub_host,
                                     pub_port=kaf_pub_port)
        else:
            warn("Kafka details not provided")

        if gcp_sub_topic and gcp_pub_topic:
            self.gcp = GCPPubSub(sub_topic=gcp_sub_topic,
                                 pub_topic=gcp_pub_topic,
                                 fn=self._gcp_message_in,
                                 project=gcp_project)
        else:
            warn("GCP details not provided")

    def subscribe(self):
        """Subscribers start to listen"""
        try:
            self.kafka.subscribe()
        except NameError:
            pass

        try:
            self.gcp.subscribe()
        except NameError:
            pass

    def publish(self, output, to_kafka=True, to_gcp=True):
        """Publishes output"""
        if to_kafka:
            self.kafka.publish(output)
        if to_gcp:
            self.gcp.publish(output)

    def _gcp_message_in(self, message):
        # Format the data
        message.ack()
        data = json.loads(message.data)
        self.fn(data)

    def _kafka_message_in(self, message):
        data = json.loads(message.value)
        self.fn(data)


class StreamingPubSub(object):

    def __init__(self, sub_topic, pub_topic, fn):
        """
        sub_topic: the topic to subscribe to
        pub_topic: the topic to publish topic
        fn: the function to pass the message to
        """
        self.sub_topic = sub_topic
        self.pub_topic = pub_topic
        self.subscriber = None
        self.publisher = None
        self.fn = fn

    def submit_job(self, *args, **kwargs):
        return self.fn(*args, **kwargs)

    def subscribe(self):
        """
        Read from `sub_topic`
        """
        raise NotImplementedError

    def publish(self):
        """
        Publish to `pub_topic`

        out_dict: the key value store to publish
        """
        raise NotImplementedError


class KafkaPubSub(StreamingPubSub):

    def __init__(self, sub_topic, pub_topic, fn,
                 sub_host="localhost", sub_port=9092,
                 pub_host="localhost", pub_port=9092):
        """
        Initialises a subscriber and publisher for Kafka

        sub_topic: the topic to subscribe to
        pub_topic: the topic to publish topic
        fn: the function to pass the message to
        sub_host: the address to the host to subscribe to
        sub_port: the port of the `sub_host` to connect to
        pub_host: the address of the host to publish to
        pub_port: the port of the `pub_host` to connect to
        """
        super().__init__(sub_topic, pub_topic, fn)

        self.subscriber = KafkaConsumer(self.sub_topic,
                                    bootstrap_servers=f"{sub_host}:{sub_port}")
        self.publisher = KafkaProducer(
                        bootstrap_servers=f"{pub_host}:{pub_port}",
                        value_serializer=lambda v: \
                                          json.dumps(v).encode('utf-8'))

    @threaded
    def subscribe(self):
        for msg in self.subscriber:
            self.fn(msg)


    def publish(self, out_dict):
        self.publisher.send(self.pub_topic, value=out_dict)


class GCPPubSub(StreamingPubSub):

    def __init__(self, sub_topic, pub_topic, fn,
                 project="vector-ai-challenge",
                 service_account_file=os.environ["GCP_SERVICE_ACCOUNT_FILE"]):
        """
        Initialises a subscriber and publisher for GCP PubSub

        sub_topic: the subscription to pull from
        pub_topic: the topic to publish to
        fn: the function to pass the message to
        project: the GCP project to work on
        service_account_file: the path to service-account-info.json
        """
        self.project = project
        pub_topic = f"projects/{self.project}/topics/{pub_topic}"

        super().__init__(sub_topic, pub_topic, fn)

        sub_creds, pub_creds = gcp_auth(service_account_file)

        self.subscriber = pubsub_v1.SubscriberClient(credentials=sub_creds)
        self.publisher = pubsub_v1.PublisherClient(credentials=pub_creds)

    def subscribe(self):
        """Subscribe to the topic in another thread"""
        subscription_path = self.subscriber.subscription_path(self.project,
                                                              self.sub_topic)
        future = self.subscriber.subscribe(subscription_path,
                                           self.fn)

    def publish(self, out_dict):
        self.publisher.publish(self.pub_topic, str.encode(json.dumps(out_dict)))
