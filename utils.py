import threading
import json

from google.auth import jwt

PUBLISHER_AUDIENCE = "https://pubsub.googleapis.com/google.pubsub.v1.Publisher"
SUBSCRIBER_AUDIENCE = "https://pubsub.googleapis.com/google.pubsub.v1.Subscriber"


def threaded(fn):
    def wrapper(*args, **kwargs):
        thread = threading.Thread(target=fn, args=args, kwargs=kwargs)
        thread.start()
        return thread
    return wrapper

def gcp_auth(service_account_info_file):
    service_account_info = json.load(open(service_account_info_file))
    sub_creds = jwt.Credentials.from_service_account_info(
        service_account_info, audience=SUBSCRIBER_AUDIENCE
    )

    pub_creds = sub_creds.with_claims(audience=PUBLISHER_AUDIENCE)
    return sub_creds, pub_creds
