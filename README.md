# Tech Challenge for Vector.ai

**See solution notes at the bottom**

## Part 1

- Build a multi-class image classifier on the ​ fashion MNIST dataset using
a Convolutional Neural Network (CNN) based model.
- Your library will need to be flexible enough to train an image classifier
on a different dataset and be written in a way to allow anyone to use it.
We will try to run it internally on our own dataset using instructions you
provide. Make it as easy as possible for us to use it.
- Don’t spend too long trying to push the accuracy up.
- Feel free to use open source code / guidelines.

## Part 2

- Build a unified API in python to send and receive messages to / from
Apache Kafka​ and ​ Google Pub/Sub​ .
- You will have to choose the appropriate client libraries.
- The inputs to the function and the outputs should be as unified as
possible.

## Part 3

- Let’s do something real world now! We have multiple machine learning
services that are coordinated via a message broker.
Here, you have to design and build a robust system to classify fashion
images. (Here, we can use the fashion MNIST validation set to mock
the input) The system will have a single client consuming a single
machine learning service.
- Use the model from Part 1 and the library from Part 2 to build such an
application. It does have to be robust, scalable and able to process
requests asynchronously.
- Note that this is not a REST API based system but rather one which
can process requests in a non-blocking way and (theoretically) put the
results somewhere else (like a database). You can mock this by
printing to the console.

## Solution notes

- In order to connect to GCPs PubSub, you will need to set an environment variable `GCP_SERVICE_ACCOUNT_FILE` which is the path to a service account authentication file. There are a number of ways this auth could be done, but this was the simplest way without making any assumptions about your current setup.

- You will also need to create a topic in the GCP console, and also a subscription to that topic. For Kafka you only need to create a topic. My local testing setup did so following steps 1-5 [here](https://kafka.apache.org/quickstart).

- Requirements should be installed using `pip install -r requirements.txt`.

- The system can be started by calling `python pubsub_classifier.py`, which will automatically download & train on FashionMNIST with preset arguments and then subscribe to the specificed topics.

- FashionMNIST is utilised using `torchvision.datasets.FashionMNIST`, custom datasets can be a configured as a root directory containt a `train` and `test` directory, where each is  configured in line with `torchvision.datasets.ImageFolder`.

- Inference is carried by providing a local filepath or a http(s) URL.

- Inputs to topics should be structured as follows:

```python
{
    "image_id": 1234,
    "fp": "https://media.mnn.com/assets/images/2019/06/rescue_cat_photo.jpg.653x0_q80_crop-smart.jpg"
}
```
and the output will return as
```python
{
    "image_id": 1234,
    "label": "Bag"
}
```

- custom datasets should be a top level directory containing two directories `train` and `test`, each of which should be configured in line with `torchvision.datasets.ImageFolder` (see [here](https://pytorch.org/docs/stable/torchvision/datasets.html#imagefolder)). So the overall folder structure will look like this:

```
/data/custom_dataset_demo
├── test
│   ├── cat
│   │   └── filename.jpg
│   └── dog
│       └── filename.jpg
└── train
    ├── cat
    │   ├── filename.jpg
    │   └── filename.jpg
    └── dog
        ├── filename.jpg
        └── filename.jpg
```

and can be executed trained upon as

```python
    ps_classifier = PubSubClassifier("/data/custom_dataset_demo", "my_dataset",
                                     # Kafka arguments
                                     kaf_sub_host="localhost", kaf_sub_port=9092,
                                     kaf_pub_host="localhost", kaf_pub_port=9092,
                                     kaf_sub_topic="test", kaf_pub_topic="labels",
                                     # GCP arguments
                                     gcp_project="vector-ai-challenge",
                                     gcp_sub_topic="sub_test", gcp_pub_topic="pub_test",
                                     # Model arguments
                                     efficientnet_scale=0,
                                     img_channels=1,
                                     img_size=(32,32),
                                     # Training arguments
                                     epochs=15, lr=0.01, momentum=0.9, batch_size=1024,
                                     trans=None)

    ps_classifier.api.subscribe()
```
