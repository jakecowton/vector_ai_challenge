import requests
import tempfile

from classifier import Classifier
from pubsub_api import UnifiedPubSubAPI


class PubSubClassifier(object):

    def __init__(self, dataset, run_name,
                 kaf_sub_host="localhost", kaf_sub_port=9092,
                 kaf_pub_host="localhost", kaf_pub_port=9092,
                 kaf_sub_topic=None, kaf_pub_topic=None,
                 gcp_project=None, gcp_sub_topic=None, gcp_pub_topic=None,
                 efficientnet_scale=0, img_channels=1, img_size=(32,32),
                 **training_kwargs):
        """
        A streaming image classifier. Uses `pubsub_api.UnifiedPubSubAPI` to
        interface with GCP PubSub and Kafkfa topics.

        N.B. In order to interface with GCP, you need to set the
        `GCP_SERVICE_ACCOUNT_FILE` environment variable for authenticating.
        This requires a service account be set up and a JSON file be produced
        (see here: https://console.cloud.google.com/iam-admin/serviceaccounts)

        dataset: any class from `torchvision.datasets`
                 (e.g. torchvision.datasets.FashionMNIST)
                 OR a path to a custom dataset
                 This should be a path to a folder containing a 'train' and
                 'test' folder, where each is  configured in line with
                 `torchvision.datasets.ImageFolder`
        run_name: the name of the directory to load from/save to (this is
                  where training checkpoints and tensorboard data is stored)
        kaf_sub_host: the address to the kafka host to subscribe to
        kaf_sub_port: the port of the `kaf_sub_host` to connect to
        kaf_pub_host: the address of the Kafka host to publish to
        kaf_pub_port: the port of the `kaf_pub_host` to connect to
        gcp_project: the name of the project the GCP topics/subscribers are located
        gcp_sub_topic: the GCP subcsription to subscribe to
        gcp_pub_topic: the GCP topic to publish to
        efficientnet_scale: the size of the efficient to use (int, 0-7)
        img_channels: the number of channels to use in images
        img_size: the size to resize input images to (this not used if
                  `trans` is passed as part of `training_kwargs`
        training_kwargs: additional arguments for model training see
                         `Classifier.train` for valid *key word* arguments
                         (epochs, lr, momentum, batch_size, trans)
        """

        self.api = UnifiedPubSubAPI(self.classify,
                                    kaf_sub_host, kaf_sub_port,
                                    kaf_pub_host, kaf_pub_port,
                                    kaf_sub_topic, kaf_pub_topic,
                                    gcp_project,
                                    gcp_sub_topic, gcp_pub_topic)

        self.classifier = Classifier(efficientnet_scale, img_channels, img_size)
        self.classifier.train(dataset, run_name, **training_kwargs)

    def classify(self, data):
        """
        Classify an image using the trained classifier

        data: a dictionary containing the image ID and the *local* path
            to the image, for example:
                        {
                            'image_id': 9284379,
                            'fp': '/a/path/to/img.jpg'
                        }
        """
        try:
            image_id = data["image_id"]
            image_fp = data["fp"]

            if image_fp.startswith("http"):
                tf = tempfile.NamedTemporaryFile()
                img_data = requests.get(image_fp).content
                with open(tf.name, 'wb') as f:
                    f.write(img_data)
                image_fp = tf.name

            label = self.classifier.infer(image_fp)
            print(label)
            self.api.publish(self._construct_output(image_id, label))

            if image_fp.startswith("http"):
                tf.close()
        except Exception as e:
            self.api.publish(self._construct_error(e))

    def _construct_output(self, image_id, label):
        return {"image_id": image_id, "label": label}

    def _construct_error(self, error):
        return {"message": str(error)}


if __name__ == "__main__":
    from torchvision.datasets import FashionMNIST
    ps_classifier = PubSubClassifier(FashionMNIST, "FashionMNIST",
                                     kaf_sub_host="localhost", kaf_sub_port=9092,
                                     kaf_pub_host="localhost", kaf_pub_port=9092,
                                     kaf_sub_topic="test", kaf_pub_topic="labels",
                                     gcp_project="vector-ai-challenge",
                                     gcp_sub_topic="sub_test", gcp_pub_topic="pub_test",
                                     efficientnet_scale=0, img_channels=1, img_size=(32,32),
                                     epochs=15, lr=0.01, momentum=0.9, batch_size=1024,
                                     trans=None)

    ps_classifier.api.subscribe()